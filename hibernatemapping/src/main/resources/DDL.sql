CREATE TABLE `jpadb1`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  `addressId` INT ,
  PRIMARY KEY (`id`),
  INDEX `user_address_ID_FK_idx` (`addressId` ASC) VISIBLE,
  CONSTRAINT `user_address_ID_FK`
    FOREIGN KEY (`addressId`)
    REFERENCES `jpadb1`.`address` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION);
  
  CREATE TABLE `jpadb1`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `addressLine1` VARCHAR(100) NOT NULL,
  `addressLine2` VARCHAR(100) NULL,
  `city` VARCHAR(45) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
COMMENT = '		';


CREATE TABLE `jpadb1`.`carbrand` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
CREATE TABLE `vehicle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `model` varchar(45) DEFAULT NULL,
  `type` varchar(45) NOT NULL,
  `carBrandId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicle_carbrand_carbrand_id_fk_idx` (`carBrandId`),
  KEY `vehicle_user_user_id_fk_idx` (`userId`),
  CONSTRAINT `vehicle_carbrand_carbrand_id_fk` FOREIGN KEY (`carBrandId`) REFERENCES `carbrand` (`id`),
  CONSTRAINT `vehicle_user_user_id_fk` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci

CREATE TABLE `jpadb1`.`application` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

  
CREATE TABLE `jpadb1`.`applicationaccess` (
  `userId` INT NOT NULL,
  `applicationId` INT NOT NULL,
  PRIMARY KEY (`userId`, `applicationId`),
  INDEX `application_applicationaccess_id_fk_idx` (`applicationId` ASC) VISIBLE,
  CONSTRAINT `user_applicationaccess_id_fk`
    FOREIGN KEY (`userId`)
    REFERENCES `jpadb1`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `application_applicationaccess_id_fk`
    FOREIGN KEY (`applicationId`)
    REFERENCES `jpadb1`.`application` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);