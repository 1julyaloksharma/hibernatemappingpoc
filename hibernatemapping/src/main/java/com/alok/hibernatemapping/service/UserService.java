package com.alok.hibernatemapping.service;

import java.util.List;

import com.alok.hibernatemapping.dto.ApplicationDTO;
import com.alok.hibernatemapping.dto.UserDTO;
import com.alok.hibernatemapping.dto.VehicleDTO;

public interface UserService {

	public List<UserDTO> getUser(String name);
	
	public boolean createUser(UserDTO user);
	
	public boolean deleteAddress(int id);
	
	public boolean deleteUser(String name);
	
	public boolean createOrUpdateUserVehicles(List<VehicleDTO> vehicle, String username);
	
	public boolean createOrUpdateApplicationAccess(List<ApplicationDTO> applications, String username);
}
