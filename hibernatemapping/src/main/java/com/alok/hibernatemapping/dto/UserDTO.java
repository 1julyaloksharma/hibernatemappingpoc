package com.alok.hibernatemapping.dto;

import java.io.Serializable;
import java.util.Set;

import com.alok.hibernatemapping.entity.UserEntity;

public class UserDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6053717721391408355L;

	private String username;

	private String firstName;

	private String lastName;

	private AddressDTO addressDTO;

	private Set<VehicleDTO> vehicleDTO;

	private Set<ApplicationDTO> applications;

	public UserDTO(String username, String firstName, String lastName) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public AddressDTO getAddressDTO() {
		return addressDTO;
	}

	public void setAddressDTO(AddressDTO addressDTO) {
		this.addressDTO = addressDTO;
	}

	public Set getVehicleDTO() {
		return vehicleDTO;
	}
	
	public void setVehicleDTO(Set<VehicleDTO> vehicleDTO) {
		this.vehicleDTO = vehicleDTO;
	}

	public Set<ApplicationDTO> getApplications() {
		return applications;
	}

	public void setApplications(Set<ApplicationDTO> applications) {
		this.applications = applications;
	}


	public UserEntity toUserEntity() {
		UserEntity entity = new UserEntity(this.username, this.firstName, this.lastName, null);
		return entity;
	}

}
