package com.alok.hibernatemapping.dto;

import java.io.Serializable;

import com.alok.hibernatemapping.entity.AddressEntity;

public class AddressDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4626695591548295881L;

	private String addressLine1;

	private String addressLine2;

	private String city;

	private String state;

	
	public AddressDTO(String addressLine1, String addressLine2, String city, String state) {
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public AddressEntity toAddressEntity() {
		AddressEntity entity =  new AddressEntity(this.addressLine1, this.addressLine2, this.city, this.state);
		return entity;
	}

}
