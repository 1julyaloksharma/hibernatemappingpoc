package com.alok.hibernatemapping.dto;

import java.io.Serializable;

public class VehicleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7406151807351955635L;

	private String name;

	private String type;

	private String model;

	private String carBrand;

	public VehicleDTO(String name, String type, String model, String carBrand) {
		super();
		this.name = name;
		this.type = type;
		this.model = model;
		this.carBrand = carBrand;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String carBrand) {
		this.carBrand = carBrand;
	}

}
