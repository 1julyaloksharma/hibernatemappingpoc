package com.alok.hibernatemapping.dto;

import java.io.Serializable;

public class ApplicationDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2309923529131274104L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
