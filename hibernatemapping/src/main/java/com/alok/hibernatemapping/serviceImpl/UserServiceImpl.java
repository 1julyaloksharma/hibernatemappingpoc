package com.alok.hibernatemapping.serviceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alok.hibernatemapping.dto.ApplicationDTO;
import com.alok.hibernatemapping.dto.UserDTO;
import com.alok.hibernatemapping.dto.VehicleDTO;
import com.alok.hibernatemapping.entity.AddressEntity;
import com.alok.hibernatemapping.entity.ApplicationEntity;
import com.alok.hibernatemapping.entity.CarBrandEntity;
import com.alok.hibernatemapping.entity.UserEntity;
import com.alok.hibernatemapping.entity.VehicleEntity;
import com.alok.hibernatemapping.repository.AddressRepository;
import com.alok.hibernatemapping.repository.CarBrandRepository;
import com.alok.hibernatemapping.repository.UserRepository;
import com.alok.hibernatemapping.repository.VehicleRepository;
import com.alok.hibernatemapping.service.UserService;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository UserRepository;

	@Autowired
	AddressRepository addressRepository;

	@Autowired
	VehicleRepository vehicleRepository;

	@Autowired
	CarBrandRepository carBrandRepository;

	@Override
	public List<UserDTO> getUser(String name) {
		List<UserEntity> userEntities = null;
		if (name == null) {
			userEntities = UserRepository.findAll();
		} else {
			userEntities = new ArrayList<UserEntity>();
			userEntities.add(UserRepository.findByusername(name.toUpperCase()));
		}

		List<UserDTO> users = new ArrayList<UserDTO>();
		userEntities.forEach(user -> users.add(user.convertToUserDTO()));
		return users;
	}

	@Override
	public boolean createUser(UserDTO user) {

		UserEntity userEntity = user.toUserEntity();
		AddressEntity addressEntity = user.getAddressDTO().toAddressEntity();

		userEntity.setAddress(addressEntity);
		UserRepository.save(userEntity);
		return true;
	}

	@Override
	public boolean deleteAddress(int id) {
		UserEntity user = UserRepository.findByAddressId(id);
		user.setAddress(null);

		UserRepository.save(user);
		return true;
	}

	@Override
	public boolean createOrUpdateUserVehicles(List<VehicleDTO> vehicles, String username) {

		UserEntity user = UserRepository.findByusername(username.toUpperCase());
		Set<VehicleEntity> vehicleEntities = new HashSet<VehicleEntity>();

		for (VehicleDTO vehicle : vehicles) {
			CarBrandEntity carBrand = new CarBrandEntity(vehicle.getCarBrand());
			VehicleEntity vehicleEntity = new VehicleEntity(vehicle.getName(), vehicle.getModel(), vehicle.getType());
			vehicleEntity.setCarBrand(carBrand);

			vehicleEntities.add(vehicleEntity);
		}

		user.setVehicles(vehicleEntities);

		UserRepository.save(user);
		return true;
	}

	@Override
	public boolean deleteUser(String name) {
		UserEntity user = UserRepository.findByusername(name.toUpperCase());
		if (user != null) {
			UserRepository.delete(user);
		}
		return true;
	}

	@Override
	public boolean createOrUpdateApplicationAccess(List<ApplicationDTO> applications, String name) {
		UserEntity user = UserRepository.findByusername(name.toUpperCase());
		if(user != null) {
			Set<ApplicationEntity> appEntities = new HashSet<ApplicationEntity>();
			for(ApplicationDTO app : applications) {
				ApplicationEntity appEntity = new ApplicationEntity(app.getName());
				appEntities.add(appEntity);
			}
			
			user.setApplications(appEntities);
			
			UserRepository.save(user);
		}
		return true;
	}

}
