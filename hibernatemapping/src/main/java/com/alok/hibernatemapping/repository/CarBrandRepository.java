package com.alok.hibernatemapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alok.hibernatemapping.entity.CarBrandEntity;

@Repository
public interface CarBrandRepository extends JpaRepository<CarBrandEntity, Integer>{

}
