package com.alok.hibernatemapping.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alok.hibernatemapping.entity.UserEntity;

@Repository
@Transactional(propagation = Propagation.REQUIRED)
public interface UserRepository extends JpaRepository<UserEntity, Integer>{

	@Query("SELECT u FROM UserEntity u WHERE UPPER(u.username) = :username")
	public UserEntity findByusername(@Param("username") final String username);
	
	@Query("SELECT u FROM UserEntity u WHERE u.address.id = :addressId")
	public UserEntity findByAddressId(@Param("addressId") final Integer addressId);
	
}
