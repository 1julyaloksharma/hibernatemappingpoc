package com.alok.hibernatemapping.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alok.hibernatemapping.dto.ApplicationDTO;
import com.alok.hibernatemapping.dto.UserDTO;
import com.alok.hibernatemapping.dto.VehicleDTO;
import com.alok.hibernatemapping.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@PostMapping(value = "")
	public boolean createUser(@RequestBody UserDTO user) {
		
		boolean result = userService.createUser(user);
		return result;
	}
	
	@GetMapping(value = "")
	public List<UserDTO> getUser(@RequestParam(name = "name", required = false) String name){
		return userService.getUser(name);
	}
	
	@DeleteMapping(value = "")
	public boolean deleteUser(@RequestParam(name = "name") String name) {
		
		return userService.deleteUser(name);
	}
	
	@DeleteMapping(value = "/address")
	public boolean deleteAddress(@RequestParam(name = "id") int id) {
		
		return userService.deleteAddress(id);
	}
	
	@PostMapping("/vehicle")
	public boolean createOrUpdateUserVehicle(@RequestBody List<VehicleDTO> vehicles, 
			@RequestParam(name="name", required = true) String name) {
		
		return userService.createOrUpdateUserVehicles(vehicles, name);
	}
	@PostMapping("/applicationaccess")
	public boolean createOrUpdateAppicationAccess(@RequestBody List<ApplicationDTO> applications, 
			@RequestParam(name="name", required = true) String name) {
		
		return userService.createOrUpdateApplicationAccess(applications, name);
	}
	
}
