package com.alok.hibernatemapping.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.alok.hibernatemapping.dto.ApplicationDTO;

@Entity
@Table(name = "application")
public class ApplicationEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", unique = true)
	private String name;

	@ManyToMany(mappedBy = "applications")
	private Set<UserEntity> users = new HashSet<UserEntity>();

	public ApplicationEntity() {

	}
	
	public ApplicationEntity(String name) {
		this.name = name;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<UserEntity> getUsers() {
		return users;
	}

	public void setUsers(Set<UserEntity> users) {
		this.users = users;
	}

	public ApplicationDTO toApplicationDTO() {
		ApplicationDTO app = new ApplicationDTO();
		app.setName(this.name);
		
		return app;
	}
}
