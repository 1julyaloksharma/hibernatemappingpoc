package com.alok.hibernatemapping.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.alok.hibernatemapping.dto.VehicleDTO;

@Entity
@Table(name = "vehicle")
public class VehicleEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4444372927326494302L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "name", unique = true)
	private String name;

	@Column(name = "model", unique = true)
	private String model;

	@Column(name = "type", unique = true)
	private String type;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "carBrandId", referencedColumnName = "id", insertable = true, updatable = true)
	private CarBrandEntity carBrand;

	@ManyToOne(fetch = FetchType.LAZY)
	private UserEntity user;

	public VehicleEntity() {

	}
	
	public VehicleEntity(String name, String model, String type) {
		super();
		this.name = name;
		this.model = model;
		this.type = type;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CarBrandEntity getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(CarBrandEntity carBrand) {
		this.carBrand = carBrand;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public VehicleDTO toVehicleDTO() {
		VehicleDTO vehicle =  new VehicleDTO(this.name, this.model, this.type, null);
		if(this.getCarBrand() != null) {
			vehicle.setCarBrand(this.getCarBrand().getName());
		}
		return vehicle;
	}
}
