package com.alok.hibernatemapping.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.alok.hibernatemapping.dto.AddressDTO;

@Entity
@Table(name = "address")
public class AddressEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 989598539196062033L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "addressLine1", unique = true)
	private String addressLine1;

	@Column(name = "addressLine2", unique = true)
	private String addressLine2;

	@Column(name = "city", unique = true)
	private String city;

	@Column(name = "state", unique = true)
	private String state;

	@OneToOne(mappedBy = "address")
	private UserEntity user;

	public AddressEntity(){
		
	}
	public AddressEntity(String addressLine1, String addressLine2, String city, String state) {
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public AddressDTO convertToDTO() {
		AddressDTO dto = new AddressDTO(this.addressLine1, this.addressLine2, this.city, this.state);
		return dto;
	}
}
