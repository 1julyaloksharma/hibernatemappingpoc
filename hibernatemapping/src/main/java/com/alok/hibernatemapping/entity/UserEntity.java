package com.alok.hibernatemapping.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.alok.hibernatemapping.dto.ApplicationDTO;
import com.alok.hibernatemapping.dto.UserDTO;
import com.alok.hibernatemapping.dto.VehicleDTO;

@Entity
@Table(name = "user")
public class UserEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6607769065857167171L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "username", unique = true)
	private String username;

	@Column(name = "firstName", unique = true)
	private String firstName;

	@Column(name = "lastName", unique = true)
	private String lastName;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(insertable = true, updatable = true, name = "addressId", referencedColumnName = "id")
	private AddressEntity address;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", insertable = true, updatable = true, referencedColumnName = "id")
	private Set<VehicleEntity> vehicles;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "applicationaccess", joinColumns = { @JoinColumn(name = "userId") }, inverseJoinColumns = {
			@JoinColumn(name = "applicationId") })
	private Set<ApplicationEntity> applications = new HashSet<ApplicationEntity>();

	public UserEntity() {

	}

	public UserEntity(String username, String firstName, String lastName, AddressEntity address) {
		super();
		this.username = username;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public AddressEntity getAddress() {
		return address;
	}

	public void setAddress(AddressEntity address) {
		this.address = address;
	}

	public Set<VehicleEntity> getVehicles() {
		return vehicles;
	}

	public void setVehicles(Set<VehicleEntity> vehicles) {
		this.vehicles = vehicles;
	}

	public Set<ApplicationEntity> getApplications() {
		return applications;
	}

	public void setApplications(Set<ApplicationEntity> applications) {
		this.applications = applications;
	}

	public UserDTO convertToUserDTO() {
		UserDTO userDTO = new UserDTO(this.username, this.firstName, this.lastName);
		if (this.getAddress() != null) {
			userDTO.setAddressDTO(this.getAddress().convertToDTO());
		}
		if (this.getVehicles() != null) {
			Set<VehicleDTO> vehicles = new HashSet<>();
			for (VehicleEntity vehicle : this.getVehicles()) {
				vehicles.add(vehicle.toVehicleDTO());
			}
			userDTO.setVehicleDTO(vehicles);
		}
		
		if(this.getApplications() != null) {
			Set<ApplicationDTO> applications = new HashSet<ApplicationDTO>();
			for(ApplicationEntity app : this.getApplications()) {
				ApplicationDTO dto = app.toApplicationDTO();
				applications.add(dto);
			}
			
			userDTO.setApplications(applications);
		}
		return userDTO;
	}

}
